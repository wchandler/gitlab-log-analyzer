#!/opt/gitlab/embedded/bin/ruby

require 'json'
require 'optparse'

class Stats
  def self.execute
    begin
      stats = self.new
    rescue Errno::ENOENT => err
      STDERR.puts "Unable to open file: #{err}"
      return
    end

    results = stats.calc_stats
    stats.print_results(results)
  end

  def fetch_path
    @manifest = JSON.parse(File.read(Dir["/opt/gitlab/embedded/nodes/*.json"].first), :symbolize_names => true)
  end

  def initialize
    begin
      @path = fetch_path
      input = File.read(@path).split("\n")
      @data = self.parse_input(input)
      @start_time = parse_time(input.first)
      @end_time = parse_time(input.last)
    end
  end

  def parse_input(data)
    begin
      data.map{|line| JSON.parse(line, :symbolize_names => true)}
    rescue JSON::ParserError => err
      abort("Invalid JSON:\n" + err.to_s)
    end
  end

  def print_results(results)
    return if results.empty?

    widths = max_widths(results)
    headers = results
      .first
      .map do |k, v|
        if v.is_a? Integer or v.is_a? Float
          "#{k.to_s.upcase.rjust(widths[k])}"
        else
          "#{k.to_s.upcase.ljust(widths[k])}"
        end
    end.join("    ")

    puts "#{@path}"
    puts
    puts headers

    results.each do |r|
      r.each do |k, v|
        if v.is_a? Integer or v.is_a? Float
          print "#{v.to_s.rjust(widths[k])}    "
        else
          print "#{v.to_s.ljust(widths[k])}    "
        end
      end
      puts
    end
  end

  def percentile(p, nums)
    nums.sort!

    return 0 if nums.empty?
    return nums.first if nums.length == 1
    return nums.last if p == 100

    idx = p / 100.0 * (nums.length - 1)
    lower, upper = nums[idx.floor,2]
    lower + (upper - lower) * (idx - idx.floor)
  end

  def stddev(nums)
    return 0 if nums.length == 1

    mean = nums.inject(:+) / nums.length.to_f
    var_sum = nums.map{|n| (n - mean) ** 2}.inject(:+).to_f
    sample_variance = var_sum / (nums.length - 1)
    Math.sqrt(sample_variance)
  end

  def durations(objs)
    objs.map{|entry| entry[:duration]}.sort!
  end

  def parse_time(line)
    event = JSON.parse(line, :symbolize_names => true)
    event[:time]
  end

  def is_json?(first_line)
    begin
      JSON.parse(first_line, :symbolize_names => true)
      true
    rescue JSON::ParserError
      false
    end
  end

  def max_widths(results)
    widths = {}

    results.first.each_key do |key|
      key_width = key.to_s.length
      widest_val = results.max_by{|val| val[key].to_s.length}[key].to_s.length

      widths[key] = widest_val > key_width ? widest_val : key_width
    end

    widths
  end
end

class ProdLogStats < Stats
  def fetch_path
    super 
    custom_path = @manifest[:normal][:gitlab][:"gitlab-rails"][:log_directory]

    if custom_path.nil?
      @manifest[:default][:gitlab][:"gitlab-rails"][:log_directory] + "/production_json.log"
    else
      custom_path + "/production_json.log"
    end
  end

  def calc_stats
    @data
      .group_by{|entry| entry[:controller]+"#"+entry[:action]}
      .map{|controller_action, entries| {
        controller: controller_action,
        count: entries.length,
        perc99: percentile(99, durations(entries)).round(2),
        perc95: percentile(95, durations(entries)).round(2),
        stddev: stddev(durations(entries)).round(2),
        max: durations(entries).last.round(2),
        min: durations(entries).first.round(2),
        score: (percentile(99, durations(entries)).round(2) * entries.length).round(1),
        percfail: (entries.select{|entry| entry[:status] >= 500}.length.to_f / entries.length * 100.0).round(2),
      }
    }.sort_by!{|results| results[:score]}.reverse
  end
end

class ApiLogStats < Stats
  def fetch_path
    super 
    custom_path = @manifest[:normal][:gitlab][:"gitlab-rails"][:log_directory]

    if custom_path.nil?
      @manifest[:default][:gitlab][:"gitlab-rails"][:log_directory] + "/api_json.log"
    else
      custom_path + "/api_json.log"
    end
  end

  def calc_stats
    @data
      .group_by{|entry| entry[:route]}
      .map{|route, entries| {
        route: route,
        count: entries.length,
        perc99: percentile(99, durations(entries)).round(2),
        perc95: percentile(95, durations(entries)).round(2),
        stddev: stddev(durations(entries)).round(2),
        max: durations(entries).last.round(2),
        min: durations(entries).first.round(2),
        score: (percentile(99, durations(entries)).round(2) * entries.length).round(1),
        percfail: (entries.select{|entry| entry[:status] >= 500}.length.to_f / entries.length * 100.0).round(2),
      }
    }.sort_by!{|results| results[:score]}.reverse
  end
end

class GitalyLogStats < Stats
  def fetch_path
    super
    custom_path = @manifest[:normal][:gitaly][:log_directory]

    if custom_path.nil?
      @manifest[:default][:gitaly][:log_directory] + "/current"
    else
      custom_path + "/current"
    end
  end

  def parse_input(data)
    if is_json?(data.first)
      parse_json(data)
    else
      parse_unstructured(data)
    end
  end

  def parse_json(data)
    results = []

    data.map do |line|
      begin
        event = JSON.parse(line, :symbolize_names => true)

        method = event[:"grpc.method"]
        duration = event[:"grpc.time_ms"]
        code = event[:"grpc.code"]

        if method and duration
          results << {method: method, duration: duration, code: code}
        end
      rescue JSON::ParserError
      end
    end

    results
  end

  def parse_unstructured(data)
    results = []

    data.each do |line|
      if line =~ /grpc.method=(\w*)/
        method = $1
      end

      if line =~ /grpc.time_ms=([\d\.]*)/
        duration = $1.to_f
      end

      if line =~ /grpc.code=(\w*)/
        code = $1
      end

      if method and duration and code
        results << {method: method, duration: duration, code: code}
      end
    end

    results
  end

  def parse_time(line)
    if is_json?(line)
      event = JSON.parse(line, :symbolize_names => true)
      time = event[:time]
    else
      if line =~ /time=\"([^\s]+Z)\"/
        time = $1
      end
    end

    time
  end

  def calc_stats
    @data
      .group_by{|entry| entry[:method]}
      .map{|method, entries| {
        method: method,
        count: entries.length,
        perc99: percentile(99, durations(entries)).round(2),
        perc95: percentile(95, durations(entries)).round(2),
        stddev: stddev(durations(entries)).round(2),
        max: durations(entries).last.round(2),
        min: durations(entries).first.round(2),
        score: (percentile(99, durations(entries)).round(2) * entries.length).round(1),
        percfail: (entries.select{|entry| entry[:code] != "OK"}.length.to_f / entries.length * 100.0).round(2),
      }
    }.sort_by!{|results| results[:score]}.reverse
  end
end

class SidekiqLogStats < Stats
  def fetch_path
    super
    custom_path = @manifest[:normal][:gitlab][:sidekiq][:log_directory]

    if custom_path.nil?
      @manifest[:default][:gitlab][:sidekiq][:log_directory] + "/current"
    else
      custom_path + "/current"
    end
  end

  def parse_input(data)
    if is_json?(data.first)
      parse_json(data)
    else
      parse_unstructured(data)
    end
  end

  def parse_json(data)
    results = []

    data.map do |line|
      begin
        event = JSON.parse(line, :symbolize_names => true)

        worker = event[:class]
        duration = event[:duration]
        status = event[:job_status]

        if worker and duration
          results << {worker: worker, duration: duration * 1000.0, status: status}
        end
      rescue JSON::ParserError
      end
    end

    results
  end

  def parse_unstructured(data)
    results = []

    data.each do |line|
      if line =~ /TID-[\S]+\s(\S+)/
        worker = $1
      end

      if line =~ /(done):\s(\d\.\d+)/
        status = $1
        duration = $2.to_f
      elsif line =~ /(fail):\s(\d\.\d+)/
        status = $1
        duration = $2.to_f
      end

      if worker and duration and status
        results << {worker: worker, duration: duration * 1000.0, status: status}
      end
    end

    results
  end

  def parse_time(line)
    if is_json?(line)
      event = JSON.parse(line, :symbolize_names => true)
      time = event[:time]
    else
      if line =~ /^([\S]+)/
        time = $1
      end
    end

    time
  end

  def calc_stats
    @data
      .group_by{|entry| entry[:worker]}
      .map{|worker, entries| {
        worker: worker,
        count: entries.length,
        perc99: percentile(99, durations(entries)).round(2),
        perc95: percentile(95, durations(entries)).round(2),
        stddev: stddev(durations(entries)).round(2),
        max: durations(entries).last.round(2),
        min: durations(entries).first.round(2),
        score: (percentile(99, durations(entries)).round(2) * entries.length).round(1),
        percfail: (entries.select{|entry| entry[:status] != "done"}.length.to_f / entries.length * 100.0).round(2),
      }
    }.sort_by!{|results| results[:score]}.reverse
  end
end

ProdLogStats.execute
puts "\n\n----------\n\n\n"
ApiLogStats.execute
puts "\n\n----------\n\n\n"
GitalyLogStats.execute
puts "\n\n----------\n\n\n"
SidekiqLogStats.execute
